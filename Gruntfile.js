module.exports = function(grunt) {
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		
		jshint: {
			gruntfile: 'Gruntfile.js',
			dev: 'src/js/*.js'
		},

		concat: {
			dev: {
				src: ['node_modules/jquery/dist/jquery.min.js', 'src/js/*.js'], 
				dest: 'dist/js/main.js'
			},
			options: {
				'banner': '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */',
				'sourceMap': true
			},
		},

		less: {
			dev: {
				options: {
					'sourceMap': true,
					'banner': '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */'
				},
				files: {
					'dist/css/main.css': ['src/less/import.less']
				}
			},
			prod: {
				options: {
					'banner': '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */',
					'cleancss': true
				},
				files: {
					'dist/css/main.min.css': ['src/less/import.less']
				}
			}
		},

		uglify: {
			options: {
				'banner': '/*! <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %> */',
				'mangle': false
			},
			prod: {
				files: {
					'dist/js/main.min.js': ['node_modules/jquery/dist/jquery.min.js', 'src/js/*.js']
				}
			}
		},
		
		watch: {
			js: {
				files: ['src/js/*.js'], 
				tasks: ['jshint:dev', 'concat:dev']
			},
			gruntfile: {
				files: ['Gruntfile.js'],
				tasks: ['jshint:gruntfile']
			},
			less: {
				files: ['src/less/*.less'],
				tasks: ['less:dev']
			}
		}
	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-jshint');

	grunt.registerTask('default', ['jshint:dev', 'concat:dev', 'less:dev']);
	grunt.registerTask('prod', ['jshint:dev', 'uglify:prod', 'less:prod']);
};