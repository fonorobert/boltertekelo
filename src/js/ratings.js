function Rating(){
	var self = this;

	this.data = window.rating;
	this.ratingsField = '#ratings';
	this.newRatingForm = '#newRating form';

	this.build = function() {
		this.buildInitial();
		$(this.newRatingForm).submit(function(e){
			e.preventDefault();
			self.newRating($(self.newRatingForm));
		});
	};

	this.buildInitial = function() {
		var ratings = this.data;
		for (var i in ratings) {
			var rating = ratings[i];
			this.loadRating({"field": self.ratingsField, "name": rating.name, "date": rating.date, "score": rating.score, "text": rating.text});
		}
	};

	this.newRating = function(form) {
		var name = form.children('input[name="name"]').val();
		var date = new Date();
		var month = date.getMonth() + 1;
		month = (month > 9) ? month : '0' + month; 
		date = date.getFullYear() + '/' + month + '/' + date.getDate();
		var score = form.children('input[name="score"]').val();
		var text = form.children('textarea[name="text"]').val();
		var stars = this.scoreDisplay(score);

		$('#ratings h2').after('<div class="rating" tabindex="0">' + '<h3 class="name">' + name + '</h3><span class="date">' + date + '</span>' + '<div class="score">' + stars + '</div>' + '<p class="text">' + text + '</p>' + '</div>');

		$('input[name="name"], textarea').val("");
		this.scoreSet('.star[data-score="1"]');
	};

	this.loadRating = function(args) {
		args.stars = self.scoreDisplay(args.score);
		$(args.field).append('<div class="rating" tabindex="0">' + '<h3 class="name">' + args.name + '</h3><span class="date">' + args.date + '</span>' + '<div class="score">' + args.stars + '</div>' + '<p class="text">' + args.text + '</p>' + '</div>');
	};

	this.scoreDisplay = function(score) {
		var result = '';
		for (var k = 0; k < score; k++) {
			result = result + '★';
		}
		for (var j = 0; j < (5-score); j++) {
			result += '☆';
		}
		return result;
	};

	this.scoreSet = function(element) {
		var score = $(element).data('score');
		for(var i = 0; i < score; i++) {
			$('.star[data-score="' + (i+1) + '"]').html('★');
		}
		for(var k = 5; k > score; k--) {
			$('.star[data-score="' + (k) + '"]').html('☆');
		}
		$('input[name="score"]').val(score);
	};
}