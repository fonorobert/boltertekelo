var rating = new Rating();
rating.build();

function ratingOpen(element) {
	$(element).children('p.text').slideToggle();
	$(element).toggleClass('open');
}

$(document).on('keypress', '.rating', function(e){
	if(e.which === 13 || e.which === 32) {
		ratingOpen(this);
	}
});

$(document).on('click', '.rating', function(){
	ratingOpen(this);
});

$('.star').click(function(){
	rating.scoreSet(this);
});

$('.star').keypress(function(e){
	if(e.which === 13 || e.which === 32) {
		rating.scoreSet(this);
	}
});